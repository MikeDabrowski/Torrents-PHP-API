<?php
/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/
 * Date: 02.01.2017
 * Time: 22:15
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Double check if domain hasn't changed
 * This api might stop working after site update
 */

namespace API;
require_once('ApiInterface.php');
class X1337xApi implements \ApiInterface {
	private static $domain = 'http://1337x.to';

	public static function searchByTitle($keyword, $page = 1) {
		$getResults = self::getPage(self::$domain."/search/".urlencode($keyword)."/".$page."/");
		/**
		 * Previous domains:
		 */
		$results = array();
		//preg_match_all('/<div class="detName">(.*?)<\/td>/si', $getResults, $matches);
		if(!preg_match_all("/No results were returned. /si",$getResults,$out)) {
			preg_match_all('/<tr>(.*?)<\/tr>/si', $getResults, $matches);
			array_shift($matches[1]);
			foreach ($matches[1] as $result) {
				/* finds link to torrent */
				preg_match('/<a href="\/torrent\/(\d+)\/(.*?)"/si', $result, $rMatches);
				$torrentLink = preg_replace("/<a href=\"/si", self::$domain, $rMatches[0]);
				$torrentLink = rtrim($torrentLink, "\"");

				/* finds title */
				$title = rtrim($rMatches[2], "\/");

				/* finds magnet */
				preg_match('/<a href="magnet:(.*?)" title="Magnet link">/si', $result, $rMatches);
				if (empty($rMatches)) {/* this is to avoid PHP Notice: Undefined offset: 1 - that is because 1337x does not show magnet on first site */
					$rMatches = array("", "");
				}
				$magnet = "magnet:" . $rMatches[1];

				/* finds size */
				preg_match("/<td class=\"coll-4 size mob-vip\">(.*?)</si", $result, $output_array);
				$size = $output_array[1];

				/* finds uploaded date */
				preg_match("/<td class=\"coll-date\">(.*?)</si", $result, $dateLine);
				$dateLine = preg_replace("/\'/", "20", $dateLine[1]);
				$uploaded = date('d-m-Y', strtotime($dateLine));

				/* finds seeders and leechers */
				preg_match("/<td class=\"coll-2 seeds\">(.*?)<\/td>/si", $result, $rMatches);
				$seeders = $rMatches[1];
				preg_match("/<td class=\"coll-3 leeches\">(.*?)<\/td>/si", $result, $rMatches);
				$leechers = $rMatches[1];

				/* finds resolution */
				$resolution = 0;
				if (preg_match("/8K|4320|4320p/i", $title)) {
					$resolution = 4320;
				} else if (preg_match("/4K|2160|2160p/i", $title)) {
					$resolution = 2160;
				} else if (preg_match("/1080|1080p/i", $title)) {
					$resolution = 1080;
				} else if (preg_match("/720|720p/i", $title)) {
					$resolution = 720;
				} else if (preg_match("/540|540p/i", $title)) {
					$resolution = 540;
				}

				if (!empty($torrentLink)) {
					$results[] = array(
						"Title" => $title,
						"TorrentLink" => $torrentLink,
						"Magnet" => $magnet,
						"Uploaded" => $uploaded,
						"Size" => $size,
						"Seeders" => $seeders,
						"Leechers" => $leechers,
						"Resolution" => $resolution
					);
				}
				break;
			}
		}else{
			throw new \Exception("No torrents found");
		}
		return $results;
	}

	private static function getPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}