<?php
/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/
 * Date: 03.01.2017
 * Time: 13:43
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Double check if domain hasn't changed
 * This api might stop working after site update
 */

namespace API;
use \Tidy;
require_once('ApiInterface.php');
class ZooqleApi implements \ApiInterface {
	private static $domain='https://zooqle.com';
	/**
	 * Previous domains:
	 */
	public static function searchByTitle($keyword, $page = 1) {
		$page--;
		$results = array();
		$getResults = self::getPage(self::$domain."/search?q=" . urlencode($keyword));

		if (!preg_match_all("/Sorry, no torrents match your query./si",$getResults,$out)) {
			$getResults = preg_replace("/&nbsp;|&darr;/is", "", $getResults);
			$getResults = preg_replace("/<tr \"/is", "<tr ", $getResults);
			preg_match_all("/<table class=\"table table-condensed table-torrents vmiddle\"(.*?)<\/table>/si", $getResults, $output_array);
			$getResults = $output_array[0][0];
			$getResults = preg_replace("/<thead>(.*?)<\/thead>/is", "", $getResults);

			if (!$tidy = new Tidy()) {
				throw new \Error("Class Tidy not found. Make sure you have that installed.");
			} else {
				$clean = $tidy->repairString($getResults, array(
					'output-xml' => true,
					'input-xml' => true
				));
				$getResults = $clean;
			}
			$getResults = preg_replace("/\n/is", "", $getResults);

			#preg_match_all("/<a class=\" small\"href=\"(.*?)\">(.*?)<\/a>(.*?)href=\"magnet(.*?)\"(.*?)<div class=\"progress-bar(.*?)<\/div>(.*?)class=\"text-nowrap text-muted smaller\">(.*?)<td(.*?)Seeders: (.*?)\|(.*?)Leechers: (.*?)\"/si", $getResults, $rMatches);
			preg_match_all("/<a class=\" small\"href=\"(.*?)\">(.*?)<\/a>(.*?)href=\"magnet(.*?)\"(.*?)download\/(.*?)\.torrent(.*?)<div class=\"progress-bar(.*?)<\/div>(.*?)class=\"text-nowrap text-muted smaller\">(.*?)<td(.*?)Seeders: (.*?)\|(.*?)Leechers: (.*?)\"/si", $getResults, $rMatches);
			$titles = $rMatches[2];
			$magnets = $rMatches[4];
			$links = $rMatches[6];
			$sizes = $rMatches[8];
			$dates = $rMatches[10];
			$seeds = $rMatches[12];
			$leeches = $rMatches[14];

			$length = sizeof($links);
			for ($index = $length - 1; $index >= 0; $index--) {
				/* strip tags for title */
				$title = strip_tags($titles[$index]);

				/* make link */
				$link = self::$domain."/download/".$links[$index].".torrent";

				/* add magnet part */
				$magnet = "magnet" . $magnets[$index];
				/* finds resolution */
				$resolution = 0;
				if (preg_match("/8K|4320|4320p/i", $title)) {
					$resolution = 4320;
				} else if (preg_match("/4K|2160|2160p/i", $title)) {
					$resolution = 2160;
				} else if (preg_match("/1080|1080p/i", $title)) {
					$resolution = 1080;
				} else if (preg_match("/720|720p/i", $title)) {
					$resolution = 720;
				} else if (preg_match("/540|540p/i", $title)) {
					$resolution = 540;
				}
				$size = $sizes[$index];

				/* finds date */
				$now = new \DateTime('now');
				$intStr = $dates[$index];
				if ($intStr == 'long ago') {
					$interval = \DateInterval::createFromDateString('4 years');
					$dateOU = $now->sub($interval);
					$uploaded = ($dateOU->format('d-m-Y'));
				} else if (!empty($intStr)) {
					$interval = \DateInterval::createFromDateString($intStr);
					$dateOU = $now->sub($interval);
					$uploaded = ($dateOU->format('d-m-Y'));
				} else {
					$uploaded = '';
				}

				/* finds size */
				preg_match("/>(.*)$/i", $size, $output_array);
				$size = $output_array[1];
				$size = substr_replace($size, " ", -2, 0);
				$results[] = array(
					"Title" => $title,
					"TorrentLink" => $link,
					"Magnet" => $magnet,
					"Uploaded" => $uploaded,
					"Size" => $size,
					"Seeders" => $seeds[$index],
					"Leechers" => $leeches[$index],
					"Resolution" => $resolution
				);
			}
		}else{
			throw new \Exception("No torrents found");
		}
		return $results;
	}

	private static function getPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}