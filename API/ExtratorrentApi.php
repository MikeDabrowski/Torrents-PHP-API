<?php

/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/Torrents-PHP-API
 * Date: 18.12.2016
 * Time: 19:29
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Double check if domain hasn't changed
 * This api might stop working after site update
 */

namespace API;
require_once('ApiInterface.php');
class ExtratorrentApi implements \ApiInterface {
	private static $domain = 'http://extratorrent.cc';
	/**
	 * Previous domains:
	 * http://extratorrent.cc
	 */
	public static function searchByTitle($keyword, $page = 1) {
		$page--;
		$getResults = self::getPage(self::$domain . "/search/?search=" . urlencode($keyword));

		$results = array();
		//preg_match_all('/<div class="detName">(.*?)<\/td>/si', $getResults, $matches);
		if(!preg_match_all("/total <br>0</br> torrents found/si",$getResults,$out)){

			preg_match_all('/<tr class="tl.{1}">(.*?)<\/tr>/si', $getResults, $matches);

			foreach ($matches[1] as $result) {
				/* finds link to torrent */
				preg_match('/<a href="\/torrent\/(\d+)\//si', $result, $rMatches);
				$torrentID = $rMatches[1];
				$torrentLink = "http://extratorrent.cc/torrent/" . $torrentID . "/";

				/* finds title */
				preg_match('/<a href="\/torrent\/(\d*?)\/(.*?)(?!comments)" title="view (?!comments)(.*?) torrent">/si', $result, $rMatches);
				$title = $rMatches[3];

				/* finds magnet */
				preg_match('/<a href="magnet:(.*?)" title="Magnet link">/si', $result, $rMatches);
				$magnet = "magnet:" . $rMatches[1];

				/* finds size */
				preg_match("/<td>(\d*?)\.(\d{2}) (KB|MB|GB)<\/td>/si", preg_replace("/&nbsp;/i", " ", $result), $output_array);
				$size = preg_replace("/<td>|<\/td>/", "", $output_array[0]);


				/* finds uploaded date */
				$now = new \DateTime('now');
				$intStr = '';
				if (preg_match('/<td>(\d{1,2})m<\/td>/i', $result, $rMatches)) {
					$intStr = $rMatches[1] . ' minutes';
				} else if (preg_match('/<td>(\d{1,2})d<\/td>/i', $result, $rMatches)) {
					$intStr = $rMatches[1] . ' days';
				} else if (preg_match('/<td>(\d{1,2})h<\/td>/i', $result, $rMatches)) {
					$intStr = $rMatches[1] . ' hours';
				} else if (preg_match('/<td>(\d{1,2})y<\/td>/i', $result, $rMatches)) {
					$intStr = $rMatches[1] . ' years';
				}
				if (!empty($intStr)) {
					$interval = \DateInterval::createFromDateString($intStr);
					$dateOU = $now->sub($interval);
					$uploaded = ($dateOU->format('d-m-Y'));
				} else {
					$uploaded = '';
				}

				/* finds seeders and leechers */
				preg_match('/<td class="sy">(\d*?)<\/td>/i', $result, $rMatches);
				$seeders = $rMatches[1];
				preg_match('/<td class="ly">(\d*?)<\/td>/i', $result, $rMatches);
				$leechers = $rMatches[1];

				/* finds resolution */
				$resolution = 0;
				if (preg_match("/8K|4320|4320p/i", $title)) {
					$resolution = 4320;
				} else if (preg_match("/4K|2160|2160p/i", $title)) {
					$resolution = 2160;
				} else if (preg_match("/1080|1080p/i", $title)) {
					$resolution = 1080;
				} else if (preg_match("/720|720p/i", $title)) {
					$resolution = 720;
				} else if (preg_match("/540|540p/i", $title)) {
					$resolution = 540;
				}

				$results[] = array(
					"Title" => $title,
					"TorrentLink" => $torrentLink,
					"Magnet" => $magnet,
					"Uploaded" => $uploaded,
					"Size" => $size,
					"Seeders" => $seeders,
					"Leechers" => $leechers,
					"Resolution" => $resolution
				);
			}
			if(empty($results)){
				throw new \Exception("No torrents found (empty results)");
			}else {
				return $results;
			}
		}else{
			throw new \Exception("No torrents found");
		}
	}

	private function getPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}