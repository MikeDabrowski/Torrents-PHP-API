<?php

/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/
 * Date: 04.01.2017
 * Time: 22:11
 */
interface ApiInterface {
	public static function searchByTitle($keyword);
}
?>