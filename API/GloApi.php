<?php

/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/
 * Date: 07.01.2017
 * Time: 20:59
 */
namespace API;
require_once('ApiInterface.php');
class GloApi implements \ApiInterface {
	private static $domain='http://glodls.to';
	/**
	 * Previous domains:
	 */
	public static function searchByTitle($keyword, $page = 1) {
		$results = array();
		$getResults = self::getPage(self::$domain."/search_results.php?search=" . urlencode($keyword) . "&incldead=Search");

		if(!preg_match("/No torrents were found based on your search criteria./", $getResults)){
			preg_match_all("/<tr class='t-row'>(.*?)<\/tr>/si", $getResults, $rMatches);

			foreach ($rMatches[1] as $row) {
				if (!preg_match("/colspan='8'/", $row)){
					/* find title */
					preg_match_all("/<a title=('|\")(.*?)('|\") href/si", $row, $match);
					$title=$match[2][0];

					/* find dl link */
					preg_match_all("/<td class=('|\")ttable_col1('|\") align=('|\")center('|\")><a href=('|\")(.*?)('|\")>/si", $row, $match);
					$torrentLink=self::$domain.$match[6][0];

					/* find magnet */
					preg_match_all("/<a rel=('|\")nofollow('|\") href=('|\")(.*?)('|\")>/si", $row, $output_array);
					$magnet=$output_array[4][0];

					/* find size */
					preg_match_all("/center('|\")>\d+\.?\d+\s?[a-zA-Z]+</si", $row, $output_array);
					$size = ltrim(rtrim($output_array[0][0],"<"),"center'>");

					preg_match_all("/<font color.*<\/font/si", $row, $out);
					$in=$out[0][0];
					preg_match_all("/<b>(.*?)<\/b>/", $in, $sandl);
					/* find seeds */
					$seeders=preg_replace("/,/","",$sandl[1][0]);
					/* find leeches */
					$leechers = preg_replace("/,/","",$sandl[1][1]);

					/* find resolution */
					$resolution = 0;
					if (preg_match("/8K|4320|4320p/i", $title)) {
						$resolution = 4320;
					} else if (preg_match("/4K|2160|2160p/i", $title)) {
						$resolution = 2160;
					} else if (preg_match("/1080|1080p/i", $title)) {
						$resolution = 1080;
					} else if (preg_match("/720|720p/i", $title)) {
						$resolution = 720;
					} else if (preg_match("/540|540p/i", $title)) {
						$resolution = 540;
					}

					$results[] = array(
						"Title" => $title,
						"TorrentLink" => $torrentLink,
						"Magnet" => $magnet,
						"Uploaded" => '',
						"Size" => $size,
						"Seeders" => $seeders,
						"Leechers" => $leechers,
						"Resolution" => $resolution
					);
				}
			}
		}else{
			throw new \Exception("No torrents found");
		}

		return $results;
	}
	private static function getPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}