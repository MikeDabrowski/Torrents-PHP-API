<?php
/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/Torrents-PHP-API
 * Date: 31.12.2016
 * Time: 13:56
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Double check if domain hasn't changed
 * This api might stop working after site update
 */

namespace API;
require_once('ApiInterface.php');
class IsoHuntApi implements \ApiInterface {
	private static $domain='https://isohunt.to'; /* has to be with http or https. Or change it below */
	/**
	 * Previous domains:
	 *
	 */
	public static function searchByTitle($keyword,$pageNum=1){
		$getResults = self::getPage(self::$domain."/torrents/?ihq=" . urlencode($keyword) );

		preg_match_all("/<tr data-key=\"\d+\">(.*?)<\/tr>/", $getResults, $matches);
		$results = array();

		if(!preg_match_all("/<div class=\"empty\">No results found.<\/div>/is", $getResults, $output_array)){
			preg_match_all("/<tr data-key=\"\d+\">(.*?)<\/tr>/", $getResults, $matches);

			foreach ($matches[1] as $result) {
				$result=explode("<td",$result);
				/* Find Title */
				preg_match("/<span>.*<\/span>/i", $result[2], $title);
				$title = strip_tags($title[0]);



				/* Find seeders */
				preg_match("/>.*</i", $result[7], $seeders);
				$seeders = ltrim(rtrim($seeders[0],"<"),">");

				/* finds resolution */
				$resolution=0;
				if(preg_match("/8K|4320|4320p/i",$title)){
					$resolution=4320;
				}else if(preg_match("/4K|2160|2160p/i",$title)){
					$resolution=2160;
				}else if(preg_match("/1080|1080p/i",$title)){
					$resolution=1080;
				}else if(preg_match("/720|720p/i",$title)){
					$resolution=720;
				}else if(preg_match("/540|540p/i",$title)){
					$resolution=540;
				}

			    if($resolution==1080 && $seeders>0) {
				    /* Find Link to torrent */
				    preg_match("/<a href=\"\/torrent.*\"><span>/i", $result[2], $torrentLink);
				    $torrentLink = preg_replace("/<span>/", "", $torrentLink[0]);
				    $torrentLink = preg_replace("/<a href=\"/", "", $torrentLink);
				    $torrentLink = preg_replace("/\">/", "", $torrentLink);
				    $torrentLink = self::domain . $torrentLink;

				    /* Find size */
				    preg_match("/>.*</i", $result[6], $size);
				    $size = ltrim(rtrim($size[0], "<"), ">");
				    $size = preg_replace("/i/i", "", $size);

				    /* Find upload date */
				    preg_match("/>.*</i", $result[5], $date);
				    $date = date('d-m-Y', strtotime($date[0]));

				    /* Find rest */
				    $rest = self::findOtherData($torrentLink);

				    $results[] = array(
					    "Title" => $title,
					    "TorrentLink" => $torrentLink,
					    "Magnet" => $rest['Magnet'],
					    "Uploaded" => $rest['Uploaded'],
					    "Size" => $size,
					    "Seeders" => $seeders,
					    "Leechers" => $rest['Leechers'],
					    "Resolution" => $resolution
				    );
			    }
			}
		}else{
			throw new \Exception("No torrents found");
		}
		return $results;
	}
	private function findOtherData($torrentLink){
		$getResults = self::getPage($torrentLink);
		preg_match("/<a.*href=\"magnet(.*?)\"/i", $getResults, $matches);
		$magnet="magnet".$matches[1];
		preg_match("/Added.*<\/p>/i", $getResults, $output_array);
		preg_match("/\d{4}-\d{2}-\d{2}/i", $output_array[0], $output_array);
		$uploadDate=date('d-m-Y', strtotime($output_array[0]));
		preg_match("/<span class=\"leechs\">\d+<\/span>/i", $getResults, $output_array);
		preg_match("/\d+/i", $output_array[0], $output_array);
		$leechers=$output_array[0];
		return array("Magnet" => $magnet,
				"Uploaded" => $uploadDate,
				"Leechers" => $leechers);
	}
	private function getPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}