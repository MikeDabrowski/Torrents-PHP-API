<?php

/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/
 * Date: 06.01.2017
 * Time: 15:21
 */
class SearchEngine {
	public $apiInstances=array();

	/**
	 *  @param $inputSites - Array of strings of sites' names user want to search.
	 */
	public function __construct(array $inputSites=array()) {
		$this->apiInstances = array();
		if(empty($inputSites)) {
			$inputSites = $this->getClasses();
		}else{
			$this->validateInput($inputSites);
		}
		$this->importClasses($inputSites);
		foreach ($inputSites as $site) {
			$classname = 'API\\' . $site . "Api";
			$startegyInstance = new $classname();
			array_push($this->apiInstances,$startegyInstance);
		}
		$this->importClasses($inputSites);
	}
	/*===============================================================================================================MAIN SEARCH METHOD*/

	/**
	 * @param $keyword - [string] - Thing you want to find.
	 * @param $by - [title/id] - WARNING! Not all sites support search by id.
	 * @param bool $sorted - [true/false] - Sorts results by resolution, seeds, size. On top should be the best torrent.
	 * @param int $best - [number] - Sorts and shows only n best torrents. Overrides @param bool $sorted and sets it to true.
	 * @return array|int|null - array of torrents
	 */
	public function search($keyword, $by, $sorted=false, $best=0) {
		if(strtolower($by)=='title'){
			return $this->searchByTitle($keyword,$sorted,$best);
		}elseif (strtolower($by)=='id'){
			return 0;
		}
		return null;
	}
	/*===============================================================================================================CONCRETE SEARCH METHODS*/
	/**
	 * @param $keyword - [string] - Thing you want to find.
	 * @param bool $sorted - [true/false] - Sorts results by resolution, seeds, size. On top should be the best torrent.
	 * @param int $best - [number] - Sorts and shows only n best torrents. Overrides @param bool $sorted and sets it to true.
	 * @return array - array of torrents
	 */
	public function searchByTitle($keyword, $sort=false, $best=0) {
		$results = array();
		for ($i=0; $i<sizeof($this->apiInstances); $i++){
			try {
				$next = $this->apiInstances[$i]->searchByTitle($keyword);
				$results=array_merge($results,$next);
			} catch (Exception $e) {
				echo "No results from ";
				preg_match("/API\/(.*?)\.php/is",$e->getFile(),$out);
				echo $out[1].": ".$e->getMessage()."<br>";
			}
		}

		if (!$sort) {
			return $results;
		}else{
			$sorted=$this->sort($results);
			if($best==0) {
				return $sorted;
			}else{
				return array_slice($sorted,0,$best);
			}
		}
	}

	/*===============================================================================================================SORTING METHOD*/
	/**
	 * Sorts array of torrents by size, seeders and resolution.
	 * @param $arrayToSort - array of torrents to be sorted
	 * @return $arrayToSort - sorted array
	 */
	private function sort(array $arrayToSort){
		$sort=array();
		foreach ($arrayToSort as $key => $row) {
			$sort['Size'][$key]=$row['Size'];
			$sort['Seeders'][$key]=$row['Seeders'];
			$sort['Resolution'][$key]=$row['Resolution'];
		}
		array_multisort($sort['Resolution'], SORT_DESC,$sort['Seeders'], SORT_DESC,$sort['Size'], SORT_DESC, $arrayToSort);
		return $arrayToSort;
	}

	/*===============================================================================================================OTHER METHODS*/
	/**
	 * Searches API directory for API classes.
	 * @return array - Array of classes names, which can(will) be converted to instances later
	 */
	private function getClasses() {
		$inFolder = glob('API/*Api.php');
		$out = array();
		foreach ($inFolder as $item) {
			preg_match("/API\/(.*?)Api\.php/i",$item,$new);
			array_push($out,$new[1]);
		}
		return $out;
	}

	/**
	 * Checks if site provided by user has corresponding API class.
	 * @param $inputSites - sites provided by user
	 * @throws Exception - if user provided wrong site an exception is thrown
	 */
	private function validateInput($inputSites){
		$notExists = array();
		$inFolder=$this->getClasses();
		foreach ($inputSites as $site) {
			if(!in_array($site,$inFolder)){
				array_push($notExists,$site);
			}
		}
		if(!empty($notExists)){
			throw new \Exception("No api for following sites: '".implode("; ",$notExists)."'. Try again.");
		}
	}

	private function importClasses($classesToImport){
		foreach ($classesToImport as $class) {
			$imp="API/".$class."Api.php";
			require_once ($imp);
		}
	}
}