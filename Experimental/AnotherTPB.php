<?php

/**
 * Created by PhpStorm.
 * User: Mike Dabrowski
 * Git: https://gitlab.com/MikeDabrowski/
 * Date: 03.01.2017
 * Time: 20:24
 */
namespace API;
class AnotherTPB {
	private static $domain='https://thepiratebay.se';
	/**
	 * Previous domains:
	 * https://thepiratebay.se
	 */
	public static function getTorrentById($id) {
		$getTorrent = self::getPage(self::$domain."/details.php?id=" . (int) $id);
		if (strpos($getTorrent, "<h2>Not Found (aka 404)</h2>") === false) {
			preg_match('/<div id="title">(.*?)<\/div>/si', $getTorrent, $matches);
			$title = trim($matches[1]);

			preg_match("/<dt>Size:<\/dt>\n		<dd>(.*?)<\/dd>/si", $getTorrent, $matches);
			$size = str_replace("&nbsp;", " ", $matches[1]);

			preg_match("/<dt>Seeders:<\/dt>\n		<dd>(\d+)<\/dd>/si", $getTorrent, $matches);
			$seeders = $matches[1];

			preg_match("/<dt>Leechers:<\/dt>\n		<dd>(\d+)<\/dd>/si", $getTorrent, $matches);
			$leechers = $matches[1];

			preg_match("/<dt>Type:<\/dt>\n		<dd><a href=\"\/browse\/(\d+)\" title=\"More from this category\">(.*?)<\/a><\/dd>/si", $getTorrent, $matches);
			$categoryID = $matches[1];
			$category = htmlspecialchars_decode($matches[2]);

			preg_match('/<a style=\'background-image: url\("\/static\/img\/icons\/icon-magnet.gif"\);\' href="(.*?)" title="Get this torrent">/si', $getTorrent, $matches);
			$magnet = $matches[1];

			preg_match("/<dt>Info Hash:<\/dt><dd>&nbsp;<\/dd>(.*?)<\/dl>/si", $getTorrent, $matches);
			$infohash = trim($matches[1]);

			preg_match("/<div class=\"nfo\">\n<pre>(.*?)<\/pre>/si", $getTorrent, $matches);
			$description = "n/a";
			if (isset($matches[1])) {
				$description = strip_tags($matches[1]);
			}

			preg_match("/}; toggleFilelist\(\); return false;\">(\d+)<\/a><\/dd>/si", $getTorrent, $matches);
			$filecount = (int) $matches[1];

			$getFiles = self::getPage("https://thepiratebay.se/ajax_details_filelist.php?id=" . (int) $id);

			preg_match_all('/<tr><td align="left">(.*?)<\/td><td align="right">(.*?)<\/tr>/si', $getFiles, $matches);
			$files = array();
			foreach ($matches[1] as $matchNum => $match) {
				$files[$match] = str_replace("&nbsp;", " ", $matches[2][$matchNum]);
			}
			ksort($files);

			return (object) array(
				"Title" => $title,
				"Size" => $size,
				"Seeders" => $seeders,
				"Leechers" => $leechers,
				"CategoryName" => $category,
				"CategoryID" => $categoryID,
				"Magnet" => $magnet,
				"InfoHash" => $infohash,
				"Description" => $description,
				"FileCount" => $filecount,
				"Files" => $files
			);
		}
		else {
			return (object) array("Error" => "Torrent not found");
		}
	}

	public static function searchByTitle($keyword, $page = 1) {
		$page--;
		$results = array();
		$getResults = self::getPage(self::$domain."/search/" . urlencode($keyword) . "/" . $page . "/7/0/");
		//$getResults=preg_match("/<table>(.*?)<\/table>/is",$getResults);
		preg_match_all("/<table(.*?)>(.*?)<\/table>/si", $getResults, $output_array);
		$getResults=$output_array[0][0];

		preg_match_all("/<a href=\"\/torrent\/(\d+)\/(.*?)\">(.*?)<\/a>(.*?)href=\"magnet(.*?)\"(.*?)\"detDesc\">Uploaded(.*?), Size(.*?), ULed by(.*?)\">(.*?)<\/a><\/font>(.*?)right\">(.*?)<\/td>(.*?)right\">(.*?)<\/td>(.*?)<center>(.*?)<\/center>/si", $getResults, $output_array);
		$torrentIDs=$output_array[1];
		$titles=$output_array[3];
		$magnets=$output_array[5];
		$dates=$output_array[7];
		$sizes=$output_array[8];
		$uploadedBys=$output_array[10];
		$seeds=$output_array[12];
		$leeches=$output_array[14];
		$cats=$output_array[16];

		$length = sizeof($torrentIDs);
		for($index=$length-1;$index>=0;$index--){
			$title=$titles[$index];
			$torrentID=$torrentIDs[$index];
			$uploaded=preg_replace("/&nbsp;/i","-",$dates[$index]);
			$size=preg_replace("/&nbsp;/i"," ",$sizes[$index]);

			preg_match_all("/\">(.*?)<\/a/", $cats[$index], $catName);
			$category=$catName[1][0]." > ".$catName[1][1];
			preg_match_all("/\d+/", $cats[$index], $catId);
			$categoryID=$catId[0][1];

			$resolution=0;
			if(preg_match("/8K|4320|4320p/i",$title)){
				$resolution=4320;
			}else if(preg_match("/4K|2160|2160p/i",$title)){
				$resolution=2160;
			}else if(preg_match("/1080|1080p/i",$title)){
				$resolution=1080;
			}else if(preg_match("/720|720p/i",$title)){
				$resolution=720;
			}else if(preg_match("/540|540p/i",$title)){
				$resolution=540;
			}

			$results[] = array(
				"Title" => $title,
				"TorrentLink" => self::$domain."/torrent/".$torrentID."/".$title,
				"Magnet" => "magnet".$magnets[$index],
				"Uploaded" => $uploaded,
				"Size" => $size,
				"Seeders" => $seeds[$index],
				"Leechers" => $leeches[$index],
				"Resolution" => $resolution,
				"TorrentID" => $torrentIDs[$index],
				"UploadedBy" => $uploadedBys[$index],
				"Category" => $category,
				"CategoryID" => $categoryID
			);
		}

		return $results;
	}

	private static function getPage($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}