# API
## Table of contents
1. [Installation and prerequisites](#installation-and-prerequisites)
2. [How to use](#how-to-use)
3. [API format](#api-format)
4. [Currently supported sites](#currently-supported-sites)
5. [Deprecated sites](#deprecated-sites)
6. [Planned sites](#planned-sites)
7. [Abandoned sites](#abandoned-sites)
8. [Details about sites](#details-about-sites)
    1. [Extratorrent](#extratorrent)
    2. [IsoHunt](#isohunt)
    3. [1337x](#1337x)
    4. [Zooqle](#zooqle)
9. [Proxies](#proxies)
    1. [Extratorrent](#extratorrent-1)
    2. [ThePirateBay](#thepiratebay)
10. [Experimental](#experimental)
11. [Extras](#extras)
12. [TODO list](#todo-list)

---
## Installation and prerequisites
### Installation
1. Put API directory and SearchEngine.php class in your project
2. Import SearchEngine using `require_once ('SearchEngine.php');`
3. Other classes will be loaded automatically

### Prerequisites
- curl for php
- tiny for php

---
## How to use
```php
<?php
try {
	$se = new SearchEngine();
	var_dump($se->search($query,'title',true,5));
} catch (Exception $e) {
    echo "Exception ".$e->getMessage();
}
?>
```
Use this code to use all API's in one call. Try/catch block is useful to check which sites are not returning any values or have errors.
Functions' arguments are specified in PHPDocs for each function. `var_dump` is used only to show the output.
This call will return 5 best torrents from [all sites](#currently-supported-sites) available.

---
## API format
My API returns data in php array (easily converted to json) in following format:
```php
<?php
array (size=8)
    'Title' => string 'Iron-man-2-2010-1080p-BrRip-x264-1-60GB-YIFY' (length=44)
    'TorrentLink' => string 'http://1337x.to/torrent/348116/Iron-man-2-2010-1080p-BrRip-x264-1-60GB-YIFY/' (length=76)
    'Magnet' => string 'magnet:' (length=7)
    'Uploaded' => string '30-05-2012' (length=10)
    'Size' => string '1.6 GB' (length=6)
    'Seeders' => string '344' (length=3)
    'Leechers' => string '54' (length=2)
    'Resolution' => int 1080
?>
```
ThePirateBay api additionaly returns:
```php
<?php
    'TorrentID' => string '7311334' (length=7)
    'UploadedBy' => string 'YIFY' (length=4)
    'Category' => string 'Video > HD - Movies' (length=19)
    'CategoryID' => string '207' (length=3)
?>
```

---
## Currently supported sites
- [ThePirateBay](https://thepiratebay.se)
- [IsoHunt](https://isohunt.to)
- [1337x.to](http://1337x.to)
- [zooqle.com](https://zooqle.com) - only TV and movies

---
## Deprecated sites
- [Extratorrent](http://extratorrent.cc) (read details below)

---
## Planned sites
For torrent sites that have no magnet or .torrent on first page of results I will have to run each time second call to go deeper.
Time can be reduced by calling deeper only for certain torrents, or, after sorting and picking up the best, only to that one.
Then time lag will be unnoticeable.

## Abandoned sites
- https://worldwidetorrents.eu/ (read details below)

You can't recover magnet from torrent name and size only. But it might be possible to recover .torrent link that way.
###### magnet
- http://glodls.to/

###### has at least dl .torrent link on first page
- http://www.sumotorrent.sx/

###### might have dl. torrent link on first page


###### no magnet/.torrent on first page
- http://www.torrentbit.net
- http://bittorrent.am/
- https://bitsnoop.com
- https://www.limetorrents.cc
- https://www.torrentfunk.com
- https://www.torlock.com
- https://torrentproject.se
- https://rarbg.to/torrents.php

---
## Details about sites
### Worldwidetorrents.eu
This site's search feature returns a lot of junk and I mean.. A LOT.

For example: querying `arrow s05s08` I got 20 results and out of that 9 was about 'Arrow' the TV series I wanted
but 3 of that were episode 6, 4 - episode 7, 2 - episode 9. NONE episode 8.

`Secret life of pets` didn't return any matching movie.

That is why I'm abandoning this page.
### Extratorrent
After being down for two or more weeks ET came back, but I think it uses some cipher that prevents my API to search their site.
The content I'm fetching is in JSON format like `{"ct":"s5hcfPMZbCdodA5rmOu2Qvka+T6VQ7F9...","iv":"f49fe3b270f072ac347a8ba88ed427bc","s":"52d3c1228abcbd80"}`
At this moment I can't do much about it. If that is really cipher, cracking that is not an option due to time.
### IsoHunt
IsoHunt does not show magnet, leeches and upload date on first result site. API has to go deeper and therefore it takes longer to fetch all data.
I added filter that goes deeper only if resolution is 1080 and seeds > 0.
### 1337x
1337x does not show magnet on first result site. Currently API ignores that. Also .torrent link is not working yet.
### Zooqle
This was tough. This site looks good but when fetched with curl it's html tags are not closed. After many hours I finally managed to get it to work.
If torrent was uploaded over approx. 3 years ago it figures as 'long ago'. I added some workaround for that, though I have not tested all cases.

---
## Proxies
### Extratorrent
1. [Source](https://proxyof.com/extratorrent-proxy/) - Not working because can't search by url.

    1. https://proxyof.com/r.php?proxy=c2l0ZW5hYmxlLmluZm8=&site=aHR0cHM6Ly9leHRyYXRvcnJlbnQuY2M=
    2. https://proxyof.com/r.php?proxy=ZnJlZXByb3h5Lmlv&site=aHR0cHM6Ly9leHRyYXRvcnJlbnQuY2M=
    3. https://proxyof.com/r.php?proxy=c2l0ZWdldC5uZXQ=&site=aHR0cHM6Ly9leHRyYXRvcnJlbnQuY2M=
    4. https://proxyof.com/r.php?proxy=ZmlsZXNkb3dubG9hZGVyLmNvbQ==&site=aHR0cHM6Ly9leHRyYXRvcnJlbnQuY2M=
    5. https://proxyof.com/r.php?proxy=ZnJlZWFuaW1lc29ubGluZS5jb20=&site=aHR0cHM6Ly9leHRyYXRvcnJlbnQuY2M=
2. [Source](http://extra.to) - Not working without js
3. [Source](http://raptorrents.com/) - Not working but I don't know why exactly.
4. http://etmirror.com/
5. http://etproxy.com/
6. http://extratorrentonline.com/
7. http://extratorrentlive.com/

### ThePirateBay
1. [Source](https://thepiratebay.org) - not working but I don't know why.

---
## Experimental
This directory contains experimental api. I wanted to check if the solution I come up for zooqle is faster than original one.
They return different times. Sometimes new one is faster, sometimes old one is.
Times measured with microtime. FFox addon measuring page loading time returns similar results.
**NEEDS BENCHMARKING TESTS**

---
## Extras
http://www.alltorrentsites.com/

---
## TODO list
- [ ] rewrite sort function